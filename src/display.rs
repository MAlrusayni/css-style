use crate::{Style, StyleUpdater};

#[derive(Clone, Debug, Copy, PartialEq, Eq, Display, From)]
pub enum Display {
    #[display(fmt = "inline")]
    Inline,
    #[display(fmt = "block")]
    Block,
    #[display(fmt = "contents")]
    Contents,
    #[display(fmt = "flex")]
    Flex,
    #[display(fmt = "grid")]
    Grid,
    #[display(fmt = "inline-block")]
    InlineBlock,
    #[display(fmt = "inline-flex")]
    InlineFlex,
    #[display(fmt = "inline-grid")]
    InlineGrid,
    #[display(fmt = "inline-table")]
    InlineTable,
    #[display(fmt = "list-item")]
    ListItem,
    #[display(fmt = "run-in")]
    RunIn,
    #[display(fmt = "table")]
    Table,
    #[display(fmt = "table-caption")]
    TableCaption,
    #[display(fmt = "table-column-group")]
    TableColumnGroup,
    #[display(fmt = "table-header-group")]
    TableHeaderGroup,
    #[display(fmt = "table-footer-group")]
    TableFooterGroup,
    #[display(fmt = "table-row-group")]
    TableRowGroup,
    #[display(fmt = "table-cell")]
    TableCell,
    #[display(fmt = "table-column")]
    TableColumn,
    #[display(fmt = "table-row")]
    TableRow,
    #[display(fmt = "none")]
    None,
    #[display(fmt = "initial")]
    Initial,
    #[display(fmt = "inherit")]
    Inherit,
}

impl StyleUpdater for Display {
    fn update_style(self, style: Style) -> Style {
        style.insert("display", self)
    }
}
