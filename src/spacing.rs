use crate::Unit;

pub trait Spacing: Default {
    type Unit: Unit + Clone;
    fn left(self, value: impl Into<Self::Unit>) -> Self;
    fn right(self, value: impl Into<Self::Unit>) -> Self;
    fn top(self, value: impl Into<Self::Unit>) -> Self;
    fn bottom(self, value: impl Into<Self::Unit>) -> Self;

    fn x(self, value: impl Into<Self::Unit>) -> Self {
        let value = value.into();
        self.left(value.clone()).right(value)
    }

    fn y(self, value: impl Into<Self::Unit>) -> Self {
        let value = value.into();
        self.top(value.clone()).bottom(value)
    }

    fn horizontal(self, value: impl Into<Self::Unit>) -> Self {
        self.y(value)
    }

    fn vertical(self, value: impl Into<Self::Unit>) -> Self {
        self.x(value)
    }

    fn all(self, value: impl Into<Self::Unit>) -> Self {
        let value = value.into();
        self.x(value.clone()).y(value)
    }

    fn zero(self) -> Self {
        Self::default().all(Self::Unit::zero())
    }

    fn half(self) -> Self {
        Self::default().all(Self::Unit::half())
    }

    fn full(self) -> Self {
        Self::default().all(Self::Unit::full())
    }
}
