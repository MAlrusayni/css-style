use crate::{unit::*, Style, StyleUpdater};

/// ```
/// use css_style::{prelude::*, Gap, unit::{px, em}};
///
/// style()
///     .gap(px(2))
///     // this can take percent value too (.e.g 40%).
///     .gap(0.4)
///     // and can take row and column each with different value
///     .gap((em(4.), em(8.)))
///     // we can also use declarative style like this:
///     .gap(Gap::row(em(4.)).and_column(em(4.)));
/// ```
#[derive(Clone, Debug, PartialEq, Display)]
pub enum Gap {
    Value(LengthPercent),
    #[display(fmt = "{} {}", _0, _1)]
    RowColumn(LengthPercent, LengthPercent),
}

impl Gap {
    pub fn row(len: impl Into<LengthPercent>) -> Self {
        Gap::RowColumn(len.into(), px(0).into())
    }

    pub fn column(len: impl Into<LengthPercent>) -> Self {
        Gap::RowColumn(px(0).into(), len.into())
    }

    pub fn and_row(self, len: impl Into<LengthPercent>) -> Self {
        match self {
            Gap::Value(val) => Gap::RowColumn(len.into(), val),
            Gap::RowColumn(_, col) => Gap::RowColumn(len.into(), col),
        }
    }

    pub fn and_column(self, len: impl Into<LengthPercent>) -> Self {
        match self {
            Gap::Value(val) => Gap::RowColumn(val, len.into()),
            Gap::RowColumn(row, _) => Gap::RowColumn(row, len.into()),
        }
    }
}

impl<T> From<T> for Gap
where
    T: Into<LengthPercent>,
{
    fn from(source: T) -> Self {
        Gap::Value(source.into())
    }
}

impl<T1, T2> From<(T1, T2)> for Gap
where
    T1: Into<LengthPercent>,
    T2: Into<LengthPercent>,
{
    fn from((row, col): (T1, T2)) -> Self {
        let row = row.into();
        let col = col.into();
        Gap::RowColumn(row, col)
    }
}

impl StyleUpdater for Gap {
    fn update_style(self, style: Style) -> Style {
        style.insert("gap", self)
    }
}
