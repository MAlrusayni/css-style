use crate::{
    calc::Calc,
    unit::{self, *},
    Spacing, Style, StyleUpdater, Unit,
};
use derive_rich::Rich;

/// ```
/// use css_style::{prelude::*, unit::px};
///
/// style()
///     // colsure style
///     .and_padding(|conf| {
///         conf.x(px(2)) // equal to conf.left(px(2)).right(px(2))
///             .y(px(4))
///     })
///     // setter method style
///     .padding((px(2), px(4)));
/// ```
#[derive(Rich, Clone, Debug, PartialEq, From, Default)]
pub struct Padding {
    #[rich(write, write(option))]
    pub top: Option<Length>,
    #[rich(write, write(option))]
    pub right: Option<Length>,
    #[rich(write, write(option))]
    pub bottom: Option<Length>,
    #[rich(write, write(option))]
    pub left: Option<Length>,
}

impl From<i32> for Padding {
    fn from(source: i32) -> Self {
        Self::default().all(px(source))
    }
}

impl From<Length> for Padding {
    fn from(source: Length) -> Self {
        Self::default().all(source)
    }
}

impl From<unit::Length> for Padding {
    fn from(source: unit::Length) -> Self {
        Self::default().all(source)
    }
}

impl From<Percent> for Padding {
    fn from(source: Percent) -> Self {
        Self::default().all(source)
    }
}

impl<X, Y> From<(X, Y)> for Padding
where
    X: Into<Length>,
    Y: Into<Length>,
{
    fn from((x, y): (X, Y)) -> Self {
        Self::default().x(x).y(y)
    }
}

impl StyleUpdater for Padding {
    fn update_style(self, style: Style) -> Style {
        style
            .try_insert("padding-top", self.top)
            .try_insert("padding-right", self.right)
            .try_insert("padding-bottom", self.bottom)
            .try_insert("padding-left", self.left)
    }
}

impl Spacing for Padding {
    type Unit = Length;

    fn left(mut self, value: impl Into<Self::Unit>) -> Self {
        self.left = Some(value.into());
        self
    }

    fn right(mut self, value: impl Into<Self::Unit>) -> Self {
        self.right = Some(value.into());
        self
    }

    fn top(mut self, value: impl Into<Self::Unit>) -> Self {
        self.top = Some(value.into());
        self
    }

    fn bottom(mut self, value: impl Into<Self::Unit>) -> Self {
        self.bottom = Some(value.into());
        self
    }
}

impl Unit for Length {
    fn zero() -> Self {
        0.0.into()
    }

    fn full() -> Self {
        1.0.into()
    }

    fn half() -> Self {
        0.5.into()
    }
}

#[derive(Clone, Debug, PartialEq, Display, From)]
pub enum Length {
    #[from]
    Length(unit::Length),
    #[from(forward)]
    Percent(Percent),
    #[display(fmt = "inherit")]
    Inherit,
}

impl From<Calc> for Length {
    fn from(source: Calc) -> Self {
        Length::Length(source.into())
    }
}
